# Fleeting plugin for Google Cloud Platform (GCP)

This is a [fleeting](https://gitlab.com/gitlab-org/fleeting/fleeting) plugin for the Google Compute Platform.

The GCP plugin can be pointed to a [zonal managed instance group](https://cloud.google.com/compute/docs/instance-groups/create-zonal-mig), allowing
tasks executed with `fleeting` to be assigned to autoscaled instances.

[![Pipeline Status](https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-googlecloud/badges/main/pipeline.svg)](https://gitlab.com/gitlab-org/fleeting/fleeting-plugin-googlecloud/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gitlab-org/fleeting/fleeting-plugin-googlecloud)](https://goreportcard.com/report/gitlab.com/gitlab-org/fleeting/fleeting-plugin-googlecloud)

## Build the plugin

To generate the binary:

1. Ensure `$GOPATH/bin` is on your PATH.
1. Run:

```shell
cd cmd/fleeting-plugin-googlecloud/
go install 

If you manage go versions with asdf, run this command after you generate the binary:

```shell
asdf reshim
```

## Plugin configuration

The following parameters are supported:

| Parameter             | Type   | Description |
|-----------------------|--------|-------------|
| `name`                | string | Name of the instance group. |
| `project`             | string | Project that contains the instance group. |
| `zone`                | string | Zone that contains the instance group. |
| `credentials_file`    | string | Optional. Path to Google Cloud credentials. By default, the [Application Default Credentials](https://cloud.google.com/docs/authentication/application-default-credentials#GAC) are used. |
| `endpoint`            | string | Optional. Overrides default API endpoint. |

### Default connector configuration

The default connector:

- Provides sensible defaults for connecting to GCP instances
- Allows flexibility for different operating systems and authentication methods (SSH for Linux and WinRM for Windows)

| Parameter                | Default  |
|--------------------------|----------|
| `os`                     | `linux`  |
| `username`               | `Administrator` when the `winrm` protocol is used. `fleeting` when the `ssh` protocol is used. |
| `protocol`               | `ssh` or `winrm` if Windows OS is detected. |
| `use_static_credentials` | `false`  |

For Windows instances, if `use_static_credentials` is false, the password field is populated with a password that GCP provisions.

For other instances, if `use_static_credentials` is false and the protocol is `ssh`, a new key pair is generated and added to the instance's metadata.

## Instance Group Setup

- Use a "managed instance group (stateless)"
- For "Autoscaling mode", the Autoscaling Configuration must be deleted
- Minimum number of instances must be 0
- Default action on failure is recommended to be "No action"
- Updates during VM instance repair should use "Update the instance configuration" to apply updates only on VM creation

NOTE: When the default action on failure is "No action", stopped instances (for example, terminated spot instances) will be removed
rather than restarted. When set to "Repair instance", the instance will be automatically restarted by the instance group.
This may not always be ideal, for example, if you're using Runner/taskscaler and have a `max_use_count` of 1, then the instance
will be re-created only to be instantly removed.

## Required Permissions

A service account with the following roles and permissions is required:

Roles:

- `iam.serviceAccountUser`

Permissions:

- `compute.instanceGroupManagers.get`
- `compute.instanceGroupManagers.update`
- `compute.instances.get`
- `compute.instances.setMetadata`

## WinRM

The fleeting connector can use Basic authentication through WinRM-HTTP (TCP/5985) to connect to the GCE instance.

The following startup script (`windows-startup-script-ps1`) enables a WinRM connection:

```powershell
winrm quickconfig -q -force
winrm set winrm/config/service/Auth '@{Basic="true"}'
winrm set winrm/config '@{MaxTimeoutms="7200000"}'
winrm set winrm/config '@{MaxEnvelopeSizekb="8192"}'
winrm set winrm/config/winrs '@{MaxMemoryPerShellMB="0"}'
winrm set winrm/config/winrs '@{MaxProcessesPerShell="0"}'
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/service '@{MaxConcurrentOperationsPerUser="12000"}'
New-NetFirewallRule -DisplayName "Allow inbound WinRM" -Direction Inbound -LocalPort 5985-5986 -Protocol TCP -Action Allow

# Optionally configure UAC to allow privilege elevation in remote shells
$key = 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System'
$setting = 'LocalAccountTokenFilterPolicy'
Set-ItemProperty -Path $key -Name $setting -Value 1 -Force
```

This adjusts the firewall, and allows Basic authentication through an unencrypted connection (WinRM-HTTP).

## Examples

### GitLab Runner

For examples on using this plugin, see the [Instance executor](https://docs.gitlab.com/runner/executors/instance.html#examples) and [Docker Autoscaler executor](https://docs.gitlab.com/runner/executors/docker_autoscaler.html#examples) documentation.
