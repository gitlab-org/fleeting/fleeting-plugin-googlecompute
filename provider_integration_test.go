package googlecloud

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"io"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/fleeting/fleeting/integration"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
	"google.golang.org/api/compute/v1"
)

/*
The following permissions are needed to create and delete an instance group:

	role: iam.serviceAccountUser

	permissions:
	compute.disks.create
	compute.instanceGroupManagers.create
	compute.instanceGroupManagers.delete
	compute.instanceGroups.create
	compute.instanceGroups.delete
	compute.instanceTemplates.get
	compute.instanceTemplates.useReadOnly
	compute.instances.create
	compute.instances.setMetadata
	compute.subnetworks.use
	compute.subnetworks.useExternalIp
*/
func TestProvisioning(t *testing.T) {
	project := os.Getenv("GCP_PROJECT")
	if project == "" {
		t.Skip("no integration test project defined")
	}

	zone := os.Getenv("GCP_ZONE")
	if zone == "" {
		t.Skip("no integration test zone defined")
	}

	templateURL := os.Getenv("GCP_INSTANCE_TEMPLATE_URL")
	if templateURL == "" {
		t.Skip("no integration test instance template url defined")
	}

	service, err := compute.NewService(context.Background())
	require.NoError(t, err)

	name := uniqueASGName()
	op, err := service.InstanceGroupManagers.Insert(project, zone, &compute.InstanceGroupManager{
		Name:             name,
		InstanceTemplate: templateURL,
		TargetSize:       0,
		ForceSendFields:  []string{"TargetSize"},
	}).Do()
	require.NoError(t, err)

	wait(t, service, project, zone, op.Name)

	defer func() {
		op, err = service.InstanceGroupManagers.Delete(project, zone, name).Do()
		require.NoError(t, err)
		wait(t, service, project, zone, op.Name)
	}()

	integration.TestProvisioning(t,
		integration.BuildPluginBinary(t, "cmd/fleeting-plugin-googlecloud", "fleeting-plugin-googlecloud"),
		integration.Config{
			PluginConfig: InstanceGroup{
				Name:    name,
				Project: project,
				Zone:    zone,
			},
			ConnectorConfig: provider.ConnectorConfig{
				Timeout: 10 * time.Minute,
			},
			MaxInstances:    3,
			UseExternalAddr: true,
		},
	)
}

func wait(t *testing.T, service *compute.Service, project, zone, opName string) {
	t.Helper()

	for i := 0; i < 15; i++ {
		wait, err := service.ZoneOperations.Wait(project, zone, opName).Do()
		require.NoError(t, err)

		if wait.Status == "DONE" {
			if wait.Error != nil {
				encoded, err := wait.Error.MarshalJSON()
				require.NoError(t, err)
				t.Fatal(string(encoded))
			}

			return
		}
		time.Sleep(1 * time.Second)
	}

	t.Fatalf("waiting on %s failed", opName)
}

func uniqueASGName() string {
	var buf [8]byte
	io.ReadFull(rand.Reader, buf[:])

	return "asg-fleeting-integration-" + hex.EncodeToString(buf[:])
}
