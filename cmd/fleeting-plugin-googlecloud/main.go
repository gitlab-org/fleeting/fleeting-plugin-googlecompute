package main

import (
	googlecloud "gitlab.com/gitlab-org/fleeting/fleeting-plugin-googlecloud"
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
)

func main() {
	plugin.Main(&googlecloud.InstanceGroup{}, googlecloud.Version)
}
