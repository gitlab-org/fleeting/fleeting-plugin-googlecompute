package fake

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/big"
	"slices"
	"sync"
	"sync/atomic"

	"google.golang.org/api/compute/v1"
)

type Instance struct {
	Status   string
	Metadata map[string]string
}

type Op struct {
	ID              string
	UpdateInstances map[string]Instance
}

type Client struct {
	Instances map[string]*Instance
	Ops       []Op

	count atomic.Uint64
}

var sshKeyOnce sync.Once
var sshKey *rsa.PrivateKey

func Key() *rsa.PrivateKey {
	sshKeyOnce.Do(func() {
		var err error
		sshKey, err = rsa.GenerateKey(rand.Reader, 4096)
		if err != nil {
			panic(err)
		}
	})

	return sshKey
}

func New() *Client {
	return &Client{
		Instances: make(map[string]*Instance),
	}
}

func (c *Client) ListManagedInstances(ctx context.Context, project, zone, instanceGroupManager string) (*compute.InstanceGroupManagersListManagedInstancesResponse, error) {
	instances := make([]*compute.ManagedInstance, 0, len(c.Instances))
	for name, instance := range c.Instances {
		instances = append(instances, &compute.ManagedInstance{
			Instance:       "compute/v1/projects/test-project/zones/us-west-2/instances/" + name,
			InstanceStatus: instance.Status,
		})
	}

	// transition statuses after the return, to emulate some delay
	for name := range c.Instances {
		switch c.Instances[name].Status {
		case "PROVISIONING":
			c.Instances[name].Status = "RUNNING"

		case "DEPROVISIONING":
			delete(c.Instances, name)
		}
	}

	return &compute.InstanceGroupManagersListManagedInstancesResponse{
		ManagedInstances: instances,
	}, nil
}

func (c *Client) CreateInstances(ctx context.Context, project, zone, instanceGroupManager string, req *compute.InstanceGroupManagersCreateInstancesRequest) (*compute.Operation, error) {
	var op Op
	op.ID = fmt.Sprintf("op-%d", c.count.Add(1))

	op.UpdateInstances = make(map[string]Instance)
	for _, instance := range req.Instances {
		op.UpdateInstances[instance.Name] = Instance{Status: "PROVISIONING"}
	}

	c.Ops = append(c.Ops, op)

	return &compute.Operation{Name: op.ID}, nil
}

func (c *Client) DeleteInstances(ctx context.Context, project, zone, instanceGroupManager string, req *compute.InstanceGroupManagersDeleteInstancesRequest) (*compute.Operation, error) {
	var op Op
	op.ID = fmt.Sprintf("op-%d", c.count.Add(1))

	op.UpdateInstances = make(map[string]Instance)
	for _, instance := range req.Instances {
		op.UpdateInstances[instance] = Instance{Status: "DEPROVISIONING"}
	}

	c.Ops = append(c.Ops, op)

	return &compute.Operation{Name: op.ID}, nil
}

func (c *Client) OpWait(ctx context.Context, project, zone, operation string) (*compute.Operation, error) {
	idx := slices.IndexFunc(c.Ops, func(o Op) bool {
		return o.ID == operation
	})

	if idx == -1 {
		return nil, fmt.Errorf("operation not found")
	}

	op := c.Ops[idx]

	// add or update instance
	for name, update := range op.UpdateInstances {
		instance := c.Instances[name]
		if instance == nil {
			instance = &Instance{}
			c.Instances[name] = instance
		}

		if update.Status != "" {
			instance.Status = update.Status
		}

		if instance.Metadata == nil {
			instance.Metadata = make(map[string]string)
		}
		for key, value := range update.Metadata {
			instance.Metadata[key] = value
		}
	}

	slices.Delete(c.Ops, idx, idx)

	return &compute.Operation{Name: operation, Status: "DONE"}, nil
}

func (c *Client) GetInstance(ctx context.Context, project, zone, instance string) (*compute.Instance, error) {
	inst := c.Instances[instance]
	if inst == nil {
		return nil, fmt.Errorf("not found")
	}

	var md compute.Metadata
	for key, value := range inst.Metadata {
		v := value
		md.Items = append(md.Items, &compute.MetadataItems{Key: key, Value: &v})
	}

	return &compute.Instance{
		Name:   instance,
		Status: inst.Status,
		NetworkInterfaces: []*compute.NetworkInterface{
			{NetworkIP: "127.0.0.1"},
		},
		Metadata: &md,
	}, nil
}

func (c *Client) SetInstanceMetadata(ctx context.Context, project, zone, instance string, metadata *compute.Metadata) (*compute.Operation, error) {
	var op Op
	op.ID = fmt.Sprintf("op-%d", c.count.Add(1))

	inst := Instance{Metadata: make(map[string]string)}
	for _, item := range metadata.Items {
		inst.Metadata[item.Key] = *item.Value
	}
	op.UpdateInstances = map[string]Instance{instance: inst}

	c.Ops = append(c.Ops, op)

	return &compute.Operation{Name: op.ID}, nil
}

func (c *Client) GetSerialPortOutput(ctx context.Context, project, zone, instance string, port int64) (*compute.SerialPortOutput, error) {
	inst := c.Instances[instance]
	if inst == nil {
		return nil, fmt.Errorf("not found")
	}

	var key struct {
		ExpireOn string `json:"ExpireOn"`
		Exponent string `json:"Exponent"`
		Modulus  string `json:"Modulus"`
		UserName string `json:"UserName"`
	}

	if err := json.Unmarshal([]byte(inst.Metadata["windows-keys"]), &key); err != nil {
		return nil, fmt.Errorf("unmarshaling windows key: %w", err)
	}

	exponentBytes, _ := base64.StdEncoding.DecodeString(key.Exponent)
	modulusBytes, _ := base64.StdEncoding.DecodeString(key.Modulus)
	exponent := big.NewInt(0).SetBytes(exponentBytes)
	modulus := big.NewInt(0).SetBytes(modulusBytes)

	privKey := &rsa.PrivateKey{
		PublicKey: rsa.PublicKey{
			N: modulus,
			E: int(exponent.Int64()),
		},
	}

	encryptedPassword, err := rsa.EncryptOAEP(sha1.New(), rand.Reader, &privKey.PublicKey, []byte("password"), nil)
	if err != nil {
		return nil, fmt.Errorf("encrypting password")
	}

	encodedPassword, _ := json.Marshal(struct {
		EncryptedPassword string `json:"encryptedPassword,omitempty"`
		Modulus           string `json:"modulus,omitempty"`
	}{
		EncryptedPassword: base64.StdEncoding.EncodeToString(encryptedPassword),
		Modulus:           key.Modulus,
	})

	return &compute.SerialPortOutput{Contents: string(encodedPassword)}, nil

}
