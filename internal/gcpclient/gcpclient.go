package gcpclient

import (
	"context"

	"google.golang.org/api/compute/v1"
	"google.golang.org/api/option"
)

type Client interface {
	ListManagedInstances(ctx context.Context, project, zone, instanceGroupManager string) (*compute.InstanceGroupManagersListManagedInstancesResponse, error)
	CreateInstances(ctx context.Context, project, zone, instanceGroupManager string, req *compute.InstanceGroupManagersCreateInstancesRequest) (*compute.Operation, error)
	DeleteInstances(ctx context.Context, project, zone, instanceGroupManager string, req *compute.InstanceGroupManagersDeleteInstancesRequest) (*compute.Operation, error)
	OpWait(ctx context.Context, project, zone, operation string) (*compute.Operation, error)
	GetInstance(ctx context.Context, project, zone, instance string) (*compute.Instance, error)
	SetInstanceMetadata(ctx context.Context, project, zone, instance string, metadata *compute.Metadata) (*compute.Operation, error)
	GetSerialPortOutput(ctx context.Context, project, zone, instance string, port int64) (*compute.SerialPortOutput, error)
}

var _ Client = (*client)(nil)

type client struct {
	service *compute.Service
}

func New(opts []option.ClientOption) (Client, error) {
	service, err := compute.NewService(context.Background(), opts...)
	if err != nil {
		return nil, err
	}

	return &client{
		service: service,
	}, nil
}

func (c *client) ListManagedInstances(ctx context.Context, project, zone, instanceGroupManager string) (*compute.InstanceGroupManagersListManagedInstancesResponse, error) {
	return c.service.InstanceGroupManagers.ListManagedInstances(project, zone, instanceGroupManager).Context(ctx).Do()
}

func (c *client) CreateInstances(ctx context.Context, project, zone, instanceGroupManager string, req *compute.InstanceGroupManagersCreateInstancesRequest) (*compute.Operation, error) {
	return c.service.InstanceGroupManagers.CreateInstances(project, zone, instanceGroupManager, req).Context(ctx).Do()
}

func (c *client) DeleteInstances(ctx context.Context, project, zone, instanceGroupManager string, req *compute.InstanceGroupManagersDeleteInstancesRequest) (*compute.Operation, error) {
	return c.service.InstanceGroupManagers.DeleteInstances(project, zone, instanceGroupManager, req).Context(ctx).Do()
}

func (c *client) OpWait(ctx context.Context, project, zone, operation string) (*compute.Operation, error) {
	return c.service.ZoneOperations.Wait(project, zone, operation).Context(ctx).Do()
}

func (c *client) GetInstance(ctx context.Context, project, zone, instance string) (*compute.Instance, error) {
	return c.service.Instances.Get(project, zone, instance).Context(ctx).Do()
}

func (c *client) SetInstanceMetadata(ctx context.Context, project, zone, instance string, metadata *compute.Metadata) (*compute.Operation, error) {
	return c.service.Instances.SetMetadata(project, zone, instance, metadata).Context(ctx).Do()
}

func (c *client) GetSerialPortOutput(ctx context.Context, project, zone, instance string, port int64) (*compute.SerialPortOutput, error) {
	return c.service.Instances.GetSerialPortOutput(project, zone, instance).Port(port).Context(ctx).Do()
}
