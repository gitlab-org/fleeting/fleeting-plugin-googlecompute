package googlecloud

import (
	"context"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/hashicorp/go-hclog"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
	"google.golang.org/api/compute/v1"
	"google.golang.org/api/option"

	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-googlecloud/internal/gcpclient"
	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-googlecloud/internal/gcpclient/fake"
)

type fakeServer struct {
	srv *httptest.Server
	ig  InstanceGroup
}

func newFakeServer(t *testing.T) *fakeServer {
	srv := &fakeServer{}

	mux := http.NewServeMux()
	mux.HandleFunc("/projects/test-project/zones/us-west-2/instanceGroupManagers/my-instance-group/listManagedInstances", func(w http.ResponseWriter, r *http.Request) {
		json.NewEncoder(w).Encode(compute.InstanceGroupManagersListManagedInstancesResponse{ManagedInstances: []*compute.ManagedInstance{}})
	})

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		t.Log(r.URL.String())
	})

	srv.srv = httptest.NewServer(mux)
	srv.ig = InstanceGroup{
		Project:                 "test-project",
		Zone:                    "us-west-2",
		Name:                    "my-instance-group",
		Endpoint:                srv.srv.URL,
		testingNoAuthentication: true,
	}

	t.Cleanup(srv.srv.Close)

	return srv
}

func (fs *fakeServer) InstanceGroup() InstanceGroup {
	return fs.ig
}

func setupFakeClient(t *testing.T, setup func(client *fake.Client)) *InstanceGroup {
	t.Helper()

	oldClient := newClient
	t.Cleanup(func() {
		newClient = oldClient
	})

	newClient = func(opts []option.ClientOption) (gcpclient.Client, error) {
		client := fake.New()
		if setup != nil {
			setup(client)
		}

		return client, nil
	}

	return &InstanceGroup{
		Project: "test-project",
		Zone:    "us-west-2",
		Name:    "my-instance-group",
	}
}

func TestInit(t *testing.T) {
	t.Run("context cancels init", func(t *testing.T) {
		fs := newFakeServer(t)

		ctx, cancel := context.WithCancel(context.Background())
		cancel()

		group := fs.InstanceGroup()
		_, err := group.Init(ctx, hclog.Default(), provider.Settings{})
		require.ErrorIs(t, err, context.Canceled)
	})

	t.Run("context cancels after init still has working client", func(t *testing.T) {
		fs := newFakeServer(t)

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		group := fs.InstanceGroup()
		_, err := group.Init(ctx, hclog.Default(), provider.Settings{})
		require.NoError(t, err)

		cancel()

		err = group.Update(context.Background(), func(id string, state provider.State) {})
		require.NoError(t, err)
	})
}

func TestIncrease(t *testing.T) {
	group := setupFakeClient(t, nil)

	ctx := context.Background()

	var count int
	_, err := group.Init(ctx, hclog.Default(), provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		count++
	}))
	require.Equal(t, 0, count)

	num, err := group.Increase(ctx, 2)
	require.Equal(t, 2, num)
	require.NoError(t, err)

	count = 0
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		require.Equal(t, provider.StateCreating, state)
		count++
	}))
	require.Equal(t, 2, count)

	count = 0
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		require.Equal(t, provider.StateRunning, state)
		count++
	}))
	require.Equal(t, 2, count)
}

func TestDecrease(t *testing.T) {
	group := setupFakeClient(t, func(client *fake.Client) {
		client.Instances["pre-existing-1"] = &fake.Instance{Status: "RUNNING"}
		client.Instances["pre-existing-2"] = &fake.Instance{Status: "RUNNING"}
	})

	ctx := context.Background()

	var count int
	_, err := group.Init(ctx, hclog.NewNullLogger(), provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		require.Equal(t, provider.StateRunning, state)
		count++
	}))
	require.Equal(t, 2, count)

	removed, err := group.Decrease(ctx, []string{"pre-existing-1"})
	require.Contains(t, removed, "pre-existing-1")
	require.NoError(t, err)
	count = 0
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {
		switch id {
		case "pre-existing-1":
			require.Equal(t, provider.StateDeleted, state)
		case "pre-existing-2":
			require.Equal(t, provider.StateRunning, state)
		}
		count++
	}))
	require.Equal(t, 2, count)
}

func TestConnectInfo(t *testing.T) {
	group := setupFakeClient(t, func(client *fake.Client) {
		client.Instances["pre-existing-1"] = &fake.Instance{Status: "RUNNING"}
	})

	ctx := context.Background()

	_, err := group.Init(ctx, hclog.NewNullLogger(), provider.Settings{})
	require.NoError(t, err)
	require.NoError(t, group.Update(ctx, func(id string, state provider.State) {}))

	encodedKey := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(fake.Key()),
		},
	)

	tests := []struct {
		config provider.ConnectorConfig
		assert func(t *testing.T, info provider.ConnectInfo, err error)
	}{
		{
			config: provider.ConnectorConfig{
				OS: "linux",
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.NoError(t, err)
				require.Equal(t, info.Protocol, provider.ProtocolSSH)
				require.NotEmpty(t, info.Key)
			},
		},
		{
			config: provider.ConnectorConfig{
				Protocol: provider.ProtocolSSH,
				Key:      []byte("invalid-key"),
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.ErrorContains(t, err, "reading private key: ssh: no key found")
			},
		},
		{
			config: provider.ConnectorConfig{
				Protocol: provider.ProtocolSSH,
				Key:      encodedKey,
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.NoError(t, err)
				require.Equal(t, info.Protocol, provider.ProtocolSSH)
				require.Equal(t, info.Key, encodedKey)
				require.NotEmpty(t, info.Key)
			},
		},
		{
			config: provider.ConnectorConfig{
				Protocol: provider.ProtocolWinRM,
			},
			assert: func(t *testing.T, info provider.ConnectInfo, err error) {
				require.NoError(t, err)
				require.Equal(t, info.Protocol, provider.ProtocolWinRM)
				require.Equal(t, info.Password, "password")
			},
		},
	}

	for _, tc := range tests {
		t.Run("", func(t *testing.T) {
			group.settings.ConnectorConfig = tc.config

			info, err := group.ConnectInfo(ctx, "pre-existing-1")
			tc.assert(t, info, err)
		})
	}
}
