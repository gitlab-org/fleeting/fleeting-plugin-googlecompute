package googlecloud

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"maps"
	"path"
	"slices"
	"strings"
	"time"

	"github.com/hashicorp/go-hclog"
	"google.golang.org/api/compute/v1"
	"google.golang.org/api/option"

	"gitlab.com/gitlab-org/fleeting/fleeting-plugin-googlecloud/internal/gcpclient"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

var _ provider.InstanceGroup = (*InstanceGroup)(nil)

var newClient = gcpclient.New

type InstanceGroup struct {
	CredentialsFile string `json:"credentials_file"`
	Project         string `json:"project"`
	Zone            string `json:"zone"`
	Name            string `json:"name"`
	Endpoint        string `json:"endpoint"`

	log      hclog.Logger
	client   gcpclient.Client
	settings provider.Settings

	testingNoAuthentication bool

	instanceIDCache map[string]uint64
}

func (g *InstanceGroup) Init(ctx context.Context, log hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	g.log = log.With("project", g.Project, "zone", g.Zone, "name", g.Name)
	g.settings = settings
	g.instanceIDCache = make(map[string]uint64)

	var options []option.ClientOption
	if g.CredentialsFile != "" {
		options = append(options, option.WithCredentialsFile(g.CredentialsFile))
	}
	if g.Endpoint != "" {
		options = append(options, option.WithEndpoint(g.Endpoint))
	}
	if g.testingNoAuthentication {
		options = append(options, option.WithoutAuthentication())
	}

	var err error
	g.client, err = newClient(options)
	if err != nil {
		return provider.ProviderInfo{}, fmt.Errorf("creating client for project %q, zone %q: %w", g.Project, g.Zone, err)
	}

	return provider.ProviderInfo{
		ID:      path.Join("gce", g.Project, g.Zone, g.Name),
		MaxSize: 1000,
	}, ctx.Err()
}

func (g *InstanceGroup) Update(ctx context.Context, update func(id string, state provider.State)) error {
	list, err := g.client.ListManagedInstances(ctx, g.Project, g.Zone, g.Name)
	if err != nil {
		return err
	}

	// instance lifecycle: https://cloud.google.com/compute/docs/instances/instance-life-cycle
	// statuses: https://cloud.google.com/compute/docs/reference/rest/v1/instances#Instance.FIELDS.status
	var cleanup []string
	for _, instance := range list.ManagedInstances {
		// cleanup any stopped instances, this is needed for when repair is disabled (recommended),
		// otherwise the instances will never be removed.
		if isStopped(instance) {
			cleanup = append(cleanup, instance.Instance)
			continue
		}

		var state provider.State
		switch instance.InstanceStatus {
		case "PROVISIONING", "REPAIRING", "STAGING":
			state = provider.StateCreating
		case "DEPROVISIONING", "STOPPED", "STOPPING", "SUSPENDED", "SUSPENDING", "TERMINATED":
			state = provider.StateDeleting
		case "RUNNING":
			state = provider.StateRunning
		default:
			switch instance.CurrentAction {
			case "CREATING", "RECREATING":
				state = provider.StateCreating
			case "DELETING":
				state = provider.StateDeleting
			default:
				g.log.Warn("unknown instance status", "instance", instance.Instance, "status", instance.InstanceStatus, "action", instance.CurrentAction)
				continue
			}
		}

		// if the underlying ID of the instance has been modified, and the
		// instance is now running, we need to ensure that taskscaler knows
		// about this (periodic updates might have missed status changes)
		//
		// the easiest way to do this is explicitly update with the "deleting"
		// state first.
		if g.instanceIDCache[instance.Instance] != instance.Id && state == provider.StateRunning {
			update(instance.Instance, provider.StateDeleting)
		}

		g.instanceIDCache[instance.Instance] = instance.Id

		update(instance.Instance, state)
	}

	// remove from the instance id cache for instances that no longer exist
	maps.DeleteFunc(g.instanceIDCache, func(key string, value uint64) bool {
		return !slices.ContainsFunc(list.ManagedInstances, func(instance *compute.ManagedInstance) bool {
			return instance.Instance == key
		})
	})

	if len(cleanup) > 0 {
		err := g.delete(ctx, cleanup)
		g.log.Warn("removing instances with a stopped state", "instances", strings.Join(cleanup, ","), "err", err)
	}

	return nil
}

func (g *InstanceGroup) Increase(ctx context.Context, delta int) (int, error) {
	instances := make([]*compute.PerInstanceConfig, 0, delta)
	for i := 0; i < delta; i++ {
		var b [8]byte
		_, err := rand.Read(b[:])
		if err != nil {
			return 0, fmt.Errorf("creating random instance name: %w", err)
		}

		instances = append(instances, &compute.PerInstanceConfig{Name: g.Name + "-" + hex.EncodeToString(b[:])})
	}

	req := &compute.InstanceGroupManagersCreateInstancesRequest{Instances: instances}
	op, err := g.client.CreateInstances(ctx, g.Project, g.Zone, g.Name, req)
	if err != nil {
		return 0, err
	}

	if err := g.wait(ctx, op); err != nil {
		return 0, err
	}

	return delta, nil
}

func (g *InstanceGroup) Decrease(ctx context.Context, instances []string) ([]string, error) {
	if err := g.delete(ctx, instances); err != nil {
		return nil, err
	}

	return instances, nil
}

func (g *InstanceGroup) delete(ctx context.Context, instances []string) error {
	req := &compute.InstanceGroupManagersDeleteInstancesRequest{
		Instances:                      instances,
		SkipInstancesOnValidationError: true,
	}

	op, err := g.client.DeleteInstances(ctx, g.Project, g.Zone, g.Name, req)
	if err != nil {
		return err
	}

	if err := g.wait(ctx, op); err != nil {
		return err
	}

	return nil
}

func (g *InstanceGroup) wait(ctx context.Context, op *compute.Operation) error {
	now := time.Now()

	for {
		wait, err := g.client.OpWait(ctx, g.Project, g.Zone, op.Name)
		if err != nil {
			g.log.Error("waiting for operation", "err", err, "op", op.Name, "target", op.TargetLink, "waited", time.Since(now))
		} else {
			g.log.Info("waiting for operation", "op", op.Name, "status", wait.Status, "error", wait.Error, "waited", time.Since(now))
			if wait.Status == "DONE" {
				if wait.Error != nil {
					encoded, err := wait.Error.MarshalJSON()
					if err != nil {
						encoded = []byte(fmt.Sprintf("unable to unmarshal error: %v", err.Error()))
					}
					return fmt.Errorf("operation %q, %q in %q, %q error: %q", op.Name, op.TargetLink, g.Project, g.Zone, string(encoded))
				}
				return nil
			}
		}

		if time.Since(now) > 2*time.Minute {
			return fmt.Errorf("timeout waiting for operation %q, %q in %q, %q", op.Name, op.TargetLink, g.Project, g.Zone)
		}
		time.Sleep(time.Second)
	}
}

func (g *InstanceGroup) ConnectInfo(ctx context.Context, id string) (provider.ConnectInfo, error) {
	id = path.Base(id)
	g.log.Info("fetching instance details", "id", id)

	instance, err := g.client.GetInstance(ctx, g.Project, g.Zone, id)
	if err != nil {
		return provider.ConnectInfo{}, err
	}

	if instance.Status != "RUNNING" {
		return provider.ConnectInfo{}, fmt.Errorf("instance status is not running (%s)", instance.Status)
	}

	info := provider.ConnectInfo{ConnectorConfig: g.settings.ConnectorConfig}
	g.populateOSArch(&info, instance)
	g.populateNetwork(&info, instance)

	if info.UseStaticCredentials {
		return info, nil
	}

	if info.Protocol == "" {
		info.Protocol = provider.ProtocolSSH
		if info.OS == "windows" {
			info.Protocol = provider.ProtocolWinRM
		}
	}

	switch info.Protocol {
	case provider.ProtocolSSH:
		err = g.ssh(ctx, &info, instance)

	case provider.ProtocolWinRM:
		err = g.winrm(ctx, &info, instance)
	}
	if err != nil {
		return provider.ConnectInfo{}, err
	}

	return info, nil
}

func (g *InstanceGroup) populateOSArch(info *provider.ConnectInfo, instance *compute.Instance) {
	if info.OS == "" {
		info.OS = "linux"
		for _, disk := range instance.Disks {
			if !disk.Boot {
				continue
			}

			for _, feature := range disk.GuestOsFeatures {
				if feature.Type == "WINDOWS" {
					info.OS = "windows"
					break
				}
			}
		}
	}

	if info.Arch == "" {
		info.Arch = "amd64"
	}
}

func (g *InstanceGroup) populateNetwork(info *provider.ConnectInfo, instance *compute.Instance) {
	nic := instance.NetworkInterfaces[0]
	info.InternalAddr = nic.NetworkIP
	if len(nic.AccessConfigs) > 0 {
		info.ExternalAddr = nic.AccessConfigs[0].NatIP
	}
}

func addMetadataKeyValueList(metadata *compute.Metadata, key string, value string) {
	for _, item := range metadata.Items {
		if item.Key == key {
			*item.Value += "\n" + value
			return
		}
	}

	metadata.Items = append(metadata.Items, &compute.MetadataItems{Key: key, Value: &value})
}

func (g *InstanceGroup) Shutdown(ctx context.Context) error {
	return nil
}

// isStopped determines whether an instance has stopped
//
// It has to have no current action, be backed by no instance ID or have
// a final or unknown state.
func isStopped(instance *compute.ManagedInstance) bool {
	if instance.CurrentAction != "NONE" {
		return false
	}

	if instance.Id == 0 {
		return true
	}

	switch instance.InstanceStatus {
	case "STOPPED", "TERMINATED", "SUSPENDED", "" /* unknown */ :
		return true
	}

	return false
}
